<?php
/**
 * @file
 * Provide functions for use by hook_cron() to clean up the database.
 */

/**
 * Remove alerts from the table which were generated for users who no longer
 * have the 'access site reports' permission. Also remove any alerts generated
 * for user 1 if they are now excluded from receiving alerts.
 */
function _watchdog_alert_remove_no_access() {
  $users_with_access = db_select('users', 'u')
    ->fields('u', array('uid'));
  $users_with_access->leftJoin('users_roles', 'r', 'u.uid = r.uid');
  $users_with_access->leftJoin('role_permission', 'p', 'r.rid = p.rid');
  $users_with_access->condition('p.permission', 'access site reports');
  $delete = db_delete('watchdog_alert');
  $delete->condition('uid', $users_with_access, 'NOT IN');
  $exclude_user_1 = variable_get(
    'watchdog_alert_exclude_user_1',
    WATCHDOG_ALERT_EXCLUDE_USER_1_DEFAULT
  );
  if (!$exclude_user_1) {
    $delete->condition('uid', 1, "!=");
  }
  $delete->execute();
}

/**
 * Remove alerts from the table which no longer have an associated alert
 * profile.
 */
function _watchdog_alert_remove_no_profile() {
  $pids = db_select('watchdog_alert_profile', 'p')
    ->fields('p', array('pid'));
  db_delete('watchdog_alert')
    ->condition('pid', $pids, 'NOT IN')
    ->execute();
}

/**
 * Remove alerts from the table which were generated longer ago than the
 * configured time.
 */
function _watchdog_alert_remove_expired() {
  $expire = variable_get('watchdog_alert_expire', WATCHDOG_ALERT_EXPIRE_DEFAULT);
  if ($expire == 0) {
    return;
  }
  db_delete('watchdog_alert')
    ->condition('timestamp', time() - $expire, '<')
    ->execute();
}

/**
 * Remove alerts from the table which exceed the configured per user alert
 * limit. Preferentially remove older alerts.
 */
function _watchdog_alert_remove_over_limit() {
  $limit = variable_get(
    'watchdog_alert_store_max_user',
    WATCHDOG_ALERT_STORE_MAX_USER_DEFAULT
  );
  if ($limit == 0) {
    return;
  }
  $query = db_select('watchdog_alert', 'a')
    ->fields('a', array('uid'))
    ->groupBy('a.uid');
  $query->addExpression('COUNT(*)', 'num_alerts');
  $counts = $query->execute()->fetchAllKeyed();
  foreach ($counts as $uid => $num_alerts) {
    if ($num_alerts > $limit) {
      $first_keep = db_select('watchdog_alert', 'a')
        ->fields('a', array('aid'))
        ->condition('uid', $uid)
        ->orderBy('aid', 'DESC')
        ->range($limit - 1, 1)
        ->execute()
        ->fetchField();
      db_delete('watchdog_alert')
        ->condition('uid', $uid)
        // @todo Implement this as a sub-query if drupal fixes subselects to
        // work with other operators than just 'IN'.
        ->condition('aid', $first_keep, '<')
        ->execute();
    }
  }
}
