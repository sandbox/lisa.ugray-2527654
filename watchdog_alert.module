<?php
/**
 * @file
 * Implement hooks for the Watchdog Alert module
 */

/**
 * By default, treat user 1 as belonging to all groups.
 *
 * This is to be used with variable_get(), and if changed, it must be a value
 * found in the options in _watchdog_alert_main_settings_form().
 */
define('WATCHDOG_ALERT_EXCLUDE_USER_1_DEFAULT', FALSE);

/**
 * The default number of seconds after which alerts expire (2 weeks).
 *
 * This is to be used with variable_get(), and if changed, it must be a value
 * found in the options in _watchdog_alert_main_settings_form().
 */
define('WATCHDOG_ALERT_EXPIRE_DEFAULT', 2 * 7 * 24 * 60 * 60);

/**
 * The default number of alerts to store per user before they start being
 * cleaned up by cron.
 *
 * This is to be used with variable_get(), and if changed, it must be a value
 * found in the options in _watchdog_alert_main_settings_form().
 */
define('WATCHDOG_ALERT_STORE_MAX_USER_DEFAULT', 100);

/**
 * The default maximum number of alerts to show a user at one time.
 *
 * This is to be used with variable_get(), and if changed, it must be a value
 * found in the options in _watchdog_alert_main_settings_form().
 */
define('WATCHDOG_ALERT_DISPLAY_MAX_DEFAULT', 10);

/**
 * Implements hook_cron().
 *
 * Controls the size of the alerts table, removing entries which are no longer
 * needed.
 */
function watchdog_alert_cron() {
  module_load_include('cron.inc', 'watchdog_alert');
  _watchdog_alert_remove_no_access();
  _watchdog_alert_remove_no_profile();
  _watchdog_alert_remove_expired();
  _watchdog_alert_remove_over_limit();
}

/**
 * Implements hook_page_build().
 *
 * Creates the necessary calls to drupal_set_message(). This hook is used (and
 * not hook_init()) to ensure this takes place after any alert dismissal, and
 * after any redirects which avoids duplicate messages.
 */
function watchdog_alert_page_build(&$page) {
  global $user;
  $include_user_1 = !variable_get(
    'watchdog_alert_exclude_user_1',
    WATCHDOG_ALERT_EXCLUDE_USER_1_DEFAULT
  );
  $user_1_check = $include_user_1 || $user->uid != 1;
  if (user_access('access site reports') && $user_1_check) {
    module_load_include('admin.inc', 'watchdog_alert');
    $alerts = watchdog_alert_get_alerts($user->uid);

    foreach ($alerts as $alert) {
      _watchdog_alert_show_alert($alert);
    }
  }
}

/**
 * Implements hook_theme().
 */
function watchdog_alert_theme() {
  return array(
    'watchdog_alert_message' => array(
      'variables' => array('alert' => Null, 'severity' => Null),
      'file' => 'watchdog_alert.admin.inc',
    )
  );
}

/**
 * Implements hook_watchdog().
 *
 * Add watchdog events matching an alert profile to the database.
 */
function watchdog_alert_watchdog($log) {
  if (!watchdog_alert_is_fully_installed()) {
    return;
  }
  watchdog_alerts_add_type($log['type']);
  module_load_include('admin.inc', 'watchdog_alert');
  $profiles = _watchdog_alert_get_profiles($log['type'], $log['severity']);
  foreach ($profiles as $profile) {
    if ($profile->dismiss_for_all) {
      $uids = array(-1);
    }
    else {
      $uids = _watchdog_alert_get_uids_by_rid($profile->rid);
      $exclude_user_1 = variable_get(
        'watchdog_alert_exclude_user_1',
        WATCHDOG_ALERT_EXCLUDE_USER_1_DEFAULT
      );
      if (!$exclude_user_1) {
        $uids[] = 1;
      }
    }
    foreach ($uids as $uid) {
      _watchdog_alert_save_alert($log, $uid, $profile->pid, $profile->repeat_alert);
    }
  }
}

/**
 * Implements hook_menu().
 */
function watchdog_alert_menu() {
  $config_url = watchdog_alert_config_url();
  return array(
    $config_url => array(
      'title' => 'Watchdog Alert',
      'description' => 'Create alerts for certain kinds of logged messages.',
      'page callback' => '_watchdog_alert_profiles',
      'access arguments' => array('administer site configuration'),
      'file' => 'watchdog_alert.admin.inc',
    ),
    $config_url . '/new' => array(
      'title' => 'Create New Alert Profile',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('_watchdog_alert_edit_form'),
      'access arguments' => array('administer site configuration'),
      'file' => 'watchdog_alert.admin.inc',
    ),
    $config_url . '/%/edit' => array(
      'title' => 'Edit Alert Profile',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('_watchdog_alert_edit_form', 4),
      'access arguments' => array('administer site configuration'),
      'file' => 'watchdog_alert.admin.inc',
    ),
    $config_url . '/%/delete' => array(
      'title' => 'Delete Alert Profile',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('_watchdog_alert_delete_form', 4),
      'access arguments' => array('administer site configuration'),
      'file' => 'watchdog_alert.admin.inc',
    ),
    $config_url . '/%/dismiss' => array(
      'title' => 'Dismiss Alert',
      'page callback' => '_watchdog_alert_dismiss_page',
      'page arguments' => array(4),
      'access arguments' => array('access site reports'),
      'file' => 'watchdog_alert.admin.inc',
    ),
  );
}

/**
 * @return string
 *   The configuration URL from the .info file.
 */
function watchdog_alert_config_url() {
  $module_info = system_get_info('module', 'watchdog_alert');
  return $module_info['configure'];
}

/**
 * Implements hook_watchdog_alert_types().
 */
function watchdog_alert_watchdog_alert_types() {
  if (function_exists('_dblog_get_message_types')) {
    $types = _dblog_get_message_types();
  }
  else {
    $types = array();
  }
  $types[] = 'watchdog_alert';
  return $types;
}

/**
 * @return string[]
 *   Keys are {watchdog_alert_type}.tid and values are the corresponding .type.
 */
function watchdog_alert_get_types() {
  return db_select('watchdog_alert_type', 't')
      ->fields('t')
      ->execute()
      ->fetchAllKeyed();
}

/**
 * Add a watchdog event type to {watchdog_alert_type}. If the type already
 * exists, it will not add a second row.
 *
 * @param string $type
 *   The type to add
 */
function watchdog_alerts_add_type($type) {
  db_merge('watchdog_alert_type')
    ->key(array('type' => $type))
    ->insertFields(array('type' => $type))
    ->execute();
}

/**
 * Implements hook_modules_enabled().
 */
function watchdog_alert_modules_enabled($modules) {
  $types = array();
  $modules = array_intersect($modules, module_implements('watchdog_alert_types'));
  foreach ($modules as $module) {
    $new_types = array_values(module_invoke($module, 'watchdog_alert_types'));
    $types = array_merge($types, $new_types);
  }
  foreach (array_unique($types) as $type) {
    watchdog_alerts_add_type($type);
  }
}

/**
 * Due to the chance for watchdog() to be called during the install phase of
 * this module before its schema is added to the database we need a way to
 * determine if the module is fully installed before we start responding to
 * invocations of hook_watchdog().
 *
 * @return bool
 *   TRUE if all watchdog_alert tables exist.
 */
function watchdog_alert_is_fully_installed() {
  module_load_include('install', 'watchdog_alert');
  foreach (array_keys(watchdog_alert_schema()) as $table){
    if (!db_table_exists($table)){
      return FALSE;
    }
  }
  return TRUE;
}
